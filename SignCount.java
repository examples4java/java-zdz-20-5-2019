/*
 * Należy zbudować program, który pobierze dowolny wpisany przez użytkownika ciąg znakowy
 * a następnie wyświetli ilość użytych znaków w tekście (włączając w to spacje). Nie używamy znaków spoza ASCII!
 * Program ma wyświetlać kod np. w ten sposób:
 * PRZYKŁADOWE DANE WEJŚCOWIE: Leniwy pies lezy na kanapie.
 * PRZYKŁADOWE DANE WYJŚCIOWE:
 * a - 3 wystąpienia
 * e - 4 wystąpienia
 * i - 3 wystąpienia
 * l - 2 wystąpienia
 * n - 3 wystąpienia
 * p - 2 wystąpienia
 * s - 1 wystąpienie
 * w - 1 wystąpienie
 * y - 2 wystąpienia
 * z - 1 wystąpienie
 */
public class SignCount {
    /*
	 * Krok 1. Stworzyć zmienną typu String i wyzerować ją
	 * Krok 2. Poprosić użytkownika o tekst
	 * Krok 3. zainicjować tablicę mającą rozmiar odpowiadający ilości
	 * wszystkich liter alfabetu (bierzemy pdo uwagę jedynie duże LUB
	 * małe); tablica typu int
	 * Krok 4. Tworzymy petlę for (lub while), ktora musi wykonac się
	 * dokładnie tyle razy, ile zostało wrpwadzonych znaków przez
	 * użytkownika
	 * Krok 4a. Wewnątrz pętli sprawdzamy każdy ze znaków w następujący
	 * sposób:
	 * - czy nalezy do przedziału małych liter (a-z) 
	 * - czy należy do przedziału dużych liter (A-Z)
	 * Jeżeli należy do któregokolwiek przydziału to obliczamy
	 * wartość liczbową danego znaku (można korzystać z talic ASCII
	 * lub obliczyć z konwersji char->int)
	 * mają numer znaku odejmujemy od niego wartośc pierwszego znaku w 
	 * alfabecie (musimy uwzględnić małe bądz dzuże A), np. 
	 * wartosc - (int)'A'; <- gdy mamy do czynienia z wielką literą
	 * obliczona wartość to numer indeksu naszej tablicy int[], w którym 
	 * wartość należy większyć
	 * Krok 5. Tworzymy pętlę for, która ma wykonac się dla wszystkich
	 * pól tablicy int[]
	 * pierwszy element w tablicy (int[0]) to nasza litera a, int[1] to b
	 * itd. Wyświetlamy odpowiednią literę (np. dla  i=0 
	 * (char)(i+(int)a) -> program wyświetli nam małą literę a
	 * liczbę wyświetleń a pobieramy z tablicy wyświetlając zawartość 
	 * np. int[i];
	 */
}