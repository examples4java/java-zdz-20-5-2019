import java.util.Scanner;

/*
 * Zadanie polega na napisaniu prostego kalkulatora. Kalkulator ma mieć następującą funkcjonalność:
 * - dodawanie
 * - odejmowanie
 * - mnożenie
 * - dzielenie
 * - potęgowanie
 * - potęgowanie przez dowolną potęgę
 * - pierwiastkowanie
 * - pierwiastkowanie przez dowolną odwróconą potęgę
 * - obliczanie modulo
 * 
 * Program powinien posiadać menu użytkownika pozwalające wybrać wskazaną opcję. Powinno też posiadać opcję wyjścia z programu
 * Dodatkowo kalkulator powinien posiadać możliwość zapisywania danych w pamięci do dalszych obliczeń (w jakiejś większej
 * tablicy,np. 1000 elementów.)
 */
public class simpleCalc {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		while(true) {
			if(!menu(scan))
				break;
		}
		System.out.print("\n\nKalkulator zakończył swoje " +
				"działanie. Dziękujemy za skorzystanie z niego");
		scan.close();
	}
	
	public static boolean menu() {
		return menu(new Scanner(System.in));
	}
	
	public static boolean menu(Scanner s) {
		System.out.print("Kalkulator. Wybierz jedną z opcji\n"+
				"1. Dodawanie\n2.Odejmowanie\n(...)\n"+
				"7. Pierwiastkowanie dowolnego stopnia\n" +
				"8. Zakończ program\n" + 
				"Twój wybór: ");
		//Scanner s = new Scanner(System.in);
		s.reset();
		int wybor=-1;
		do {
			try {
				wybor = s.nextInt();
			}
			catch(Exception e) {
				System.err.print("\nWrpowadzono złe dane!");
			}
		} while(wybor==-1);
		
		switch (wybor) {
			case 1:
				
//				break;
			case 2: 
//				break;
			case 3:
//				break;
			case 4:
//				break;
			case 5:
//				break;
			case 6:
				System.err.print("\nObsługa tego zadania będzie"+
			"dostępna w kolejnym wydaniu!\n\n");
				break;
			case 7:
				double a,b;
				System.out.print("\nPodaj liczbę do spierwiastkowania: ");
				a = s.nextDouble();
				System.out.print("\nPodaj stopień pierwiastka: ");
				b = s.nextDouble();
				System.out.print("\n Pierwiastek z liczby " + a + " stopnia "
						+ b + " wynosi: " + pierwiastek(a,b) +"\n");
				break;
			case 8:
				return false;
			default:
				System.err.print("Podałeś niepoprawne dane"+
			" Wprowadz wartość ponownie.");
				break;
		}
		return true;
	}
	
	public static double pierwiastek() {
		return pierwiastek(1, 1);
	}
	
	public static double pierwiastek(double a, double b) {
		double wartosc = Math.pow(a,1/(b*1.d));
		return wartosc;
	}
}
