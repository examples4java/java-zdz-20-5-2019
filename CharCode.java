import java.io.IOException;
import java.util.Scanner;

/*
 * Program ma umożliwiać podawanie dowolej ilości znaków i/lub liczb z zakresu 0-255; Naszym zadaniem jest 
 * wskazanie użytkownikowi jakie wartości posiadają wprowadzone znaki i/lub liczby. Jeżeli liczby bądz znaki będą
 * poza zakresem należy wyrzucić stosowny komunikat
 * 
 * DODATKOWE ZADANIE:
 * napisać funkcję, która wczyta pojedyczy znak LUB pojedyczą liczbę
 * następnie ma za zadanie sprawdzić czy użytkownik podał znak czy liczbę 
 * (zakres liczb 0-255; inne wartości mają być traktowane jako błąd)
 * na koniec funkcja ma wyświetlić znak przypisany do liczby 
 * bądz liczbę przypisaną do znaku
 */
public class CharCode {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		while(true) 
			if(!menu(scan)) 
				break;
		scan.close();
	}
	
	public static boolean menu(Scanner s) {
		print("\n\nProgram do konwersji liczb<=>znaki. Wybierz:\n"+
				"1. Konwertuj znaki na liczby\n2. Konwertuj liczby na znaki"+
				"\n3. Zakończ program.\n Twój wybór: ");
		int wartosc = s.nextInt();
		switch(wartosc) {
			case 1:
				znakLiczba(s);
				break;
			case 2:
				liczbaZnak(s);
				break;
			case 3:
				return false;
		}
		return true;
	}
	
	public static void znakLiczba(Scanner s) {
		s.nextLine();
		print("\nPodaj ciąg znakowy do przekodowania na liczby; "
				+ "\nZnaki specjalne będą pomijane: ");
		String str = s.nextLine();
		print("Kody wprowadzonych znaków: ");
		for(int i=0;i<str.length();i++) {
			print((int)str.charAt(i) + " ");
		}
	}
	
	public static void liczbaZnak(Scanner s) {
		s.nextLine();
		print("\nPodaj ciąg liczb do przekodowania na znaki; "
				+ "\nposzczególne liczby rodzielaj spacjami. "
				+ "\nLiczby poza zakresem <0-255> będą pomijane: ");
		String str = s.nextLine();
		//gdy ktoś podał liczby 97 110 90
		//tablica zawiera:
		//numbers[0] -> 97
		//numbers[1] -> 110
		//numbers[2] -> 90
		String[] numbers = str.split(" ");
		for(String n : numbers) {
			print((char)Integer.valueOf(n).intValue() + " ");
		}
;	}
	
	public static void print(String s) {
		System.out.print(s);
	}
	
	
	
	
	
	
	
//	public static String readInput() {
//		String out ="";
//		int c = -1;
//		try {
//			while((c = System.in.read()) != 13) {
//				out+=(char)c;
//			}
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return out;
//	}
}
